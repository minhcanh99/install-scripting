#!/usr/bin/env bash
#Using for centos 6
ROOT=`dirname $(readlink -e $BASH_SOURCE)`

echo "BEGIN INSTALLING"

echo "====> COPYING bashrc"

cp assets/.bashrc ~/
source ~/.bashrc

echo "====> YUMMING IMPORTANT PACKAGES"
which emacs > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y nano emacs-nox wget curl tree man screen
fi

echo "====> COPYING emacs.d"
cp -r assets/.emacs.d ~/

echo "====> INSTALLING ADDITIONAL YUM SOURCES"
yum repolist | grep rpmforge > /dev/null 2>&1
if [ $? -eq 1 ]; then
    wget http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm && rpm -Uhv rpmforge-release*.rf.x86_64.rpm
    rm -rf rpmforge-release*.rf.x86_64.rpm
fi

echo "====> YUMMING IMPORTANT PACKAGES FROM ADDITIONAL SOURCE"
wich htop > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y htop
fi

echo "====> YUMMING DEVELOPMENT PACKAGES"
which git > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum groupinstall -y 'Development tools'
fi

#echo "====> YUMMING PROGRAMMING LANGUAGES"
#which java > /dev/null 2>&1
#if [ $? -eq 1 ]; then
#    yum install -y java-1.6.0-openjdk ruby rubygems python-devel python-setuptools
#    easy_install pip
#fi

echo "====> INSTALLING APACHE"
which httpd > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y pcre-devel openssl-devel libatomic_ops-devel
    yum install -y httpd
fi
cd $ROOT
echo "<h1>Hello, world</h1>" > /var/www/html/index.html
chkconfig --add httpd
netstat -nap | grep httpd > /dev/null 2>&1
if [ $? -ne 0 ]; then
    service httpd start
fi

echo "INSTALLING MariaDB"
which mysql > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y mysql-server mysql
    /sbin/service mysqld start
    mysql_secure_installation
    chkconfig mysqld on
fi

echo "INSTALLING PHP"
which php > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y libxml2-devel curl-devel libpng-devel libjpeg-devel freetype-devel libmcrypt-devel
    yum install -y php php-mysql php-mbstring
    service httpd restart
fi
yum install -y php-fpm
chkconfig --add php-fpm
service php-fpm start
echo "<?php echo phpinfo(); ?>" > /var/www/html/phpinfo.php

echo "INSTALLING PHPMYADMIN"
if [ ! -d /var/www/html/phpMyAdmin ]; then
    wget 'https://files.phpmyadmin.net/phpMyAdmin/4.4.11/phpMyAdmin-4.4.11-all-languages.tar.gz' -O phpMyAdmin.tar.gz
    tar xvzf phpMyAdmin.tar.gz
    mv phpMyAdmin-4.4.11-all-languages /var/www/html/phpMyAdmin
    rm -rf phpMyAdmin.tar.gz
fi
service php-fpm restart

echo "====> RUN source ~/.bashrc PLEASE"
