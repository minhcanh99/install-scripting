;;; install.el --- Package to ease installation of Elisp packages

;; Copyright (C) 2001, 2003, 2004, 2005, 2006  Stefan Monnier

;; Author: Stefan Monnier <monnier@gnu.org>

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This little package is meant to ease up the task of installing
;; third party ELisp packages.  I.e. it takes care of placing it in
;; an appropriate location, finds out what code is necessary to get
;; the package activated, sets up your .emacs file to activate the
;; package, and byte-compiles the package.

;; It should work on both single-file packages and tarballs.

;; On tarball packages, it does a bit of guess work to figure out
;; where are which files and how to use them.  This is bound to
;; fail sometimes.

;; The main entry points are the following:

;; M-x install-file RET
;;    Takes a file as an argument (an elisp file or a tarball) and installs
;;    and activates it.  This includes copying/untarring into the
;;    site-lisp area.
;;
;; M-x install-buffer RET
;;    Same thing but with a buffer (elisp or tar mode).
;;
;; M-x install-directory-inplace RET
;;    Takes a directory containing an elisp package (probably tarball you
;;    just untarred) and sets it up so it can be used (byte-compiles files,
;;    sets up autoloads), and activates it.  All of this without copying it
;;    into the site-lisp area.
;;
;; M-x install-list-packages RET
;;    A placeholder for what should become a UI that will allow you to list
;;    available/installed/activated packages, activate/deactivate them,
;;    install/uninstall, ...

;; Tested on:
;; + ProofGeneral
;; + sml-mode
;; + AUCTeX   (missed the info page)
;; + X-Symbol (as an XEmacs package)
;; + Gnus     (but doesn't install the info doc :-( )
;; + BBDB     (misses the main info page)
;; - WhizzyTeX (needs to hack the perl script and stuff :-( )
;; ? ECB
;; ? JDEE
;; ? preview-latex
;; ? VM
;; ? mmm-mode
;; ? Semantic

;; The above list should be taken with a heavy grain of salt: each bullet
;; was true at some point, but the code of install.el hs changed since and
;; the code of those packages as well.

;; The on-disk structure is as follows:
;; - there are two area: the `home' and the `site' each with their
;;   respective directory (~/lib/emacs and /usr/share/emacs/site-lisp)
;;   and file (.emacs and site-start).
;; - There is a distinction between installing and activating.
;;   Installing only places the files on disk, whereas activating sets up
;;   autoloads and friends.
;; - Activation is done on a directory by directory basis.  Each directory
;;   has an `autoloads' file.  Loading it activates the package(s)
;;   in directory.
;; - Single-file packages are placed together in the toplevel directory
;;   whereas tarball-packages are placed in their own subdirectory (so they
;;   can be activated independently).

;;; Todo:

;; - don't ask whether to activate site-wide packages installed in home.
;; - Create Info from Texinfo when needed.
;; - Try harder to find Info files such as doc/auctex.
;; - UI to (un)install and (de)activate packages, get a list, ...
;; - If a single-file package lacks ;;;###autoload, try to add them
;;   based on the Commentary section or something.
;; - don't prompt for site-wide/home from within the command.  Only do that
;;   from the interactive spec, if at all.
;; - install-activate should be called install-save-activation.
;;   Better separate activation and writing the activation command.
;; - don't use (load "/foo/bar/baz/autoloads" 'install) but rather something
;;   like (install-activate "baz") together with a search path like
;;   install-package-dirs (or maybe reuse load-path for that).
;; - maybe in install-activate remember load-file-name sunless install-with-existing-fi)
    ;; Finally, byte-compile the files.
    (if install-byte-compile
        (install-byte-compile-dir))))

(defun install-dirs-of-files (files)
  "Return a list of subdirs containing elisp files."
  (let ((dirs nil)
        (ignore (regexp-opt
                  (cons
                     ;; Ignore contrib directories because they tend to contain
                     ;; either less-debugged code, or packages that might
                     ;; already be installed and can thus interfere.
                     "contrib/"
                       (let ((exts nil))
                             (dolist (ext completion-ignored-extensions exts)
                                     (if (eq (aref ext (1- (length ext))) ?/)
                                           (push ext exts))))))))
    ;; Collect the dirs that hold elisp files.
    (dolist (file files dirs)
      (let ((dir (file-name-directory file)))
        (unless (or (member dir dirs)
                        (and dir (string-match ignore dir)))
            (push dir dirs))))))

(defun install-find-elisp-dirs ()
  "Return a list of subdirs containing elisp files."
  (install-dirs-of-files (install-glob "**/*.el")))

(defun install-byte-compile-dir ()
  "Byte compile all elisp files under the current directory."
  (let ((load-path (append (mapcar (lambda (dir)
                                          (if dir
                                               (expand-file-name dir)
                                                   default-directory))
                                      (install-find-elisp-dirs))
                              load-path)))
    (byte-recompile-directory default-directory 0)))

(defun install-glob (pattern)
  (let ((res (eshell-extended-glob pattern)))
    (if (listp res) res)))

(defun install-get-activation-file ()
  "Return the file to load to activate the package.
This is usually \"./autoloads\", but it can also be \"lisp/foo-site.el\"."
  (if (file-exists-p install-autoload-file)
      install-autoload-file
    (or (car (install-glob (concat "**/" install-autoload-file)))
        (car (install-glob "**/auto-autoloads.el"))
        (car (install-glob "**/*-site.el")))))

(defun install-setup-tree ()
  (eshell-glob-initialize)
  ;; Look for elisp files.
  (let ((dirs (install-find-elisp-dirs))
        (autoload-files nil)
        (toplevel nil))
    ;; Prepare each elisp subdir and collect info along the way.
    ;; PROBLEM: if a dir that conmode))
    (save-excursion
      (let ((text (pp-to-string autoload)))
        (if (string-match "\n\\'" text)
                (setq text (substring text 0 -1)))
        (goto-char (point-min))
        (unless (re-search-forward (regexp-quote text) nil t)
            (goto-char (point-min))
              (forward-comment (point-max))
                (while (re-search-backward "^" nil t))
                  (unless (bolp) (newline))
                    ;; Pass `install' as argument to load: this both makes Emacs
                    ;; ignore the load if the file is missing and is used as a marker
                    ;; indicating that this load statement was introduced by us.
                    (insert "(load " text " 'install)\n")
                      (save-buffer))))))

;;;###autoload
(defun install-list-packages ()
  "Show the installed packages."
  (interactive)
  (dired (install-get-dir)))

;; Info files and DIR files.
;; Some of this should probably be moved to info.el.

(defconst install-info-dir "-*- Text -*-\n\
File: dirNode: TopThis is the top of the INFO tree\
\n\n* Menu:\n\n"
  "Text content of a barebones empty `info/dir' file.")

(defun install-find-info-files ()
  (let ((files (or (install-glob "**/*.info*")
                      (install-glob "**/info/*")))
        (tmp nil))
    (dolist (f files)
      (unless (or (member f '("dir" "localdir"))
                    (and (string-match "-[0-9]+" f)
                                (member (replace-match "" t t f) files))
                      (not (string-match "\\.info\\>\\|\\(\\`\\|/\\)[^.]+\\(\\'\\|\\.\\(gz\\|Z\\)\\)" f)))
        (push f tmp)))
    tmp))

(defun install-make-info ()
  "Make an info/dir file if necessary and return the info directories."
  ;; FIXME: This should create the info files from the Texinfo files
  ;; if necessary !!
  ;; Problems to do that:
  ;; - detect when necessary.  E.g. BBDB comes with an info page for
  ;;   the bbdb-filters stuff, but the main bbdb doc is in texinfo.
  ;; - figure out how to makeinfo the thing.  E.g. AucTeX comes with
  ;;   a whole bunch of Texinfo files and it's really not clear which
  ;;   is the right one.
  ;; - The info file might be there, but not found.  E.e. AucTeX has its
  ;;   page in doc/auctex.
  (le