# .bashrc

# User specific aliases and functions
alias nt="netstat -nap | grep LISTEN | grep"
alias ne="netstat -nap | grep ESTA | grep"
alias pg="ps -ef | grep"
alias ll="ls -la"
alias lsh="ls *.h"

export EDITOR=emacs

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

#PS1
#export PS1='\[\e[0;35m\][\u@\H \W]\$\[\e[0m\] '
# PS1
if [ $(id -u) -eq 0 ];
then
	# Root user
	PS1="\[\e[33;1;40m\][\u] \W \$\[\e[0m\] "
else 
	# Normal user
	PS1="[\\u@\\h: \\W]$ "
fi

#Dev
export PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin"
export PKG_CONFIG_PATH=/usr/lib/pkgconfig:/usr/local/lib/pkgconfig
export CUSTOMLIB_PATH=/customlibs
export DEPLOYER_PATH=$CUSTOMLIB_PATH
export PATH=$PATH:$CUSTOMLIB_PATH/bin:$CUSTOMLIB_PATH/sbin
export C_INCLUDE_PATH=$CUSTOMLIB_PATH/include
export CPLUS_INCLUDE_PATH=$CUSTOMLIB_PATH/include
export LIBRARY_PATH=$CUSTOMLIB_PATH/lib:$CUSTOMLIB_PATH/lib64
export LD_LIBRARY_PATH=$CUSTOMLIB_PATH/lib:$CUSTOMLIB_PATH/lib64
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$CUSTOMLIB_PATH/lib/pkgconfig/

#NGINX
export PATH=$PATH:/customlibs/nginx-1.7.2/sbin
